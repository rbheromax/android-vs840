#include vendor/lge/vs840/BoardConfigVendor.mk

TARGET_ARCH := arm
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
ARCH_ARM_HAVE_TLS_REGISTER := true
TARGET_CPU_SMP := true
TARGET_CPU_VARIANT := cortex-a8

# Partitions
TARGET_USERIMAGES_USE_EXT4 := true

# Build kernel from source
TARGET_KERNEL_SOURCE := kernel/lge/iproj
TARGET_KERNEL_CONFIG := lucid_cyanogenmod_defconfig 

BOARD_KERNEL_CMDLINE := console=ttyDCC0,115200,n8 androidboot.hardware=iproj
BOARD_KERNEL_BASE := 0x40200000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS := --ramdisk_offset 0x01800000

BOARD_CDMA_NETWORK := true

# Device Assert
TARGET_OTA_ASSERT_DEVICE := i_vzw,vs840,VS840,cayman

# Recovery
BOARD_CUSTOM_GRAPHICS := ../../../device/lge/vs840/recovery/graphics.c

BOARD_CUSTOM_RECOVERY_KEYMAPPING := ../../device/lge/vs840/recovery/recovery_keys.c

BOARD_USES_SECURE_SERVICES := true

BOARD_HAS_NO_SELECT_BUTTON := true

BOARD_UMS_LUNFILE := "/sys/class/android_usb/android0/f_mass_storage/lun/file"

BOARD_HAS_LARGE_FILESYSTEM := true

TARGET_RECOVERY_FSTAB := device/lge/vs840/fstab.iproj
